<?php require_once "include/autoload.php"?>
<!DOCTYPE html>
<html lang="en">

<!-- Head --------------------------------------------------------------------------------->
<head>
    <title>Product add</title>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.css">

    <!-- JS Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/bootstrap.bundle.js"></script>
    <script src="js/add_product.js"></script>

</head>

<body>
<!-- Header ------------------------------------------------------------------------------->
<header>
    <nav class="navbar navbar-expand-sm">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <h3 class="m-1">Product Add</h3>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item mx-1">
                <input type="submit" form="form" class="btn btn-primary" value="Save">
            </li>
            <li class="nav-item mx-1">
                <a class="btn btn-primary" href="index.php">Cancel</a>
            </li>
        </ul>
    </nav>
</header>
<!-- Body --------------------------------------------------------------------------------->

<form id="form" action="upload.php" onsubmit="return validate('form')" method="post">
    <div class="table-responsive table-borderless">
        <table class="table"><tbody>

            <!-- SKU -->
            <tr><td><label for="sku">SKU:</label></td>
            <td><input id="sku" name="sku" class="mx-2" type="text" required="required" maxlength="9"></td></tr>

            <!-- Name -->
            <tr><td><label for="name">Name:</label></td>
            <td><input id="name" name="name" class="mx-2" type="text" required="required" maxlength="40"></td></tr>

            <!-- Price -->
            <tr><td><label for="price">Price:</label></td>
            <td><input id="price" name="price" class="mx-2" type="number" step="0.01" required="required" maxlength="9"></td></tr>
        </tbody></table>
    </div>

    <?php
        $switcher = new Control\TypeSwitcher();
        $switcher->showTypeSwitcher();

        $form = new Control\Form();
        $form->showForms();
    ?>
</form>


<!-- Footer ------------------------------------------------------------------------------->
<footer>
</footer>
</body>
</html>