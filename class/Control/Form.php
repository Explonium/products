<?php


namespace Control;


class Form extends \Mysqli\Form
{
    public function showForms()
    {
        // Getting all product types and printing div with inputs for each type
        $types = $this->getTypes();
        foreach ($types as $type) {
            echo "<div id='".$type."' class='table-responsive table-borderless Product' style='display: none'>
                <table class='table'><tbody>";

            // Getting all attributes for each product type and printing them
            $attributes = $this->getAttributeNames($type);
            foreach ($attributes as $attribute){
                echo "<tr><td><label for='".$attribute['FieldName']."'>".$attribute['Name'].":</label></td>
                <td><input id='".$attribute['FieldName']."' name='".$attribute['FieldName']."' class='field' type='number' maxlength='10'></td></tr>";
            }
            echo "</tbody></table>
            </div>";
        }
    }
}