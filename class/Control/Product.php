<?php


namespace Control;


class Product extends \Mysqli\Product
{
    // This function gets an array of Product objects and calling getProducts for each product
    public function showProducts()
    {
        $products = $this->getProducts();
        foreach ($products as $product) {
            $product->show();
        }
    }

    // This function is creating a single Product object and calling uploadProduct function for each product
    public function uploadProduct($data)
    {
        $className = "\\Product\\".$data['type'];
        $product = new $className($data['sku'], $data['name'], $data['price']);
        $product->uploadProduct($data);
    }

    // This function receives an sku array and calling deleteProduct function for each sku
    public function deleteProducts($skuArray)
    {
        foreach ($skuArray as $sku => $check) {
            if ($check === "check") {
                $this->deleteProduct($sku);
            }
        }
    }
}