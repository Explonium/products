<?php


namespace Control;


class TypeSwitcher extends \Mysqli\TypeSwitcher
{
    public function showTypeSwitcher()
    {
        // This function gets all product types and prints select box wit all product types
        echo "<label for='type'>Type switcher:</label>
            <select id='type' class='text-primary btn btn-light' oninput='update_form(value);' name='type' required='required'>
                <option value='' selected>Select type</option>";
        $types = $this->getTypes();
        foreach ($types as $type) {
            echo "<option value='" . $type . "'>" . $type . "</option>";
        }
        echo "</select>";
    }
}