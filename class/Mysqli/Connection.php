<?php


namespace Mysqli;


class Connection
{
    private $servername = "localhost";
    private $username = "root";
    private $password = "";
    private $dbname = "products";

// This function is used to do a mysql query and return result
    protected function connect()
    {
        // Create connection
        return new \mysqli($this->servername, $this->username, $this->password, $this->dbname);
    }
}