<?php


namespace Mysqli;


class Form extends Connection
{
    protected function getTypes()
    {
        // Executing statement
        $sql = "SELECT types.Name FROM types ORDER BY types.Name";
        $conn = $this->connect();
        $stmt = $conn->prepare($sql);
        $stmt->bind_result($name);
        $stmt->execute();

        // Creating an array of types
        $types = Array();
        while($stmt->fetch()) {
            array_push($types, $name);
        }

        return $types;
    }

    protected function getAttributeNames($type)
    {
        // Executing statement
        $sql = "SELECT Name, Field_name FROM attribute_names WHERE Type = GET_type_id(?)";
        $conn = $this->connect();
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $type);
        $stmt->bind_result($name, $fieldName);
        $stmt->execute();

        // Creating an array of attribute names
        $names = Array();
        while($stmt->fetch()) {
            array_push($names, Array('Name' => $name, 'FieldName' => $fieldName));
        }

        return $names;
    }
}