<?php
// This class is only for a mysql queries

namespace Mysqli;


class Product extends Connection
{
    protected function setProduct($sku, $name, $price, $type)
    {
        // Executing statement
        $sql = "INSERT INTO products VALUES(?, ?, ?, GET_type_id(?))";
        $conn = $this->connect();
        $stmt = $conn->prepare($sql);
        $stmt->bind_param('ssss',$sku, $name, $price, $type);
        $stmt->execute();
    }

    protected function deleteProduct($sku)
    {
        // Executing statement
        $sql = "DELETE FROM products WHERE products.SKU = ?";
        $conn = $this->connect();
        $stmt = $conn->prepare($sql);
        $stmt->bind_param('s',$sku);
        $stmt->execute();
    }

    protected function getProducts()
    {
        // Executing statement
        $sql = "SELECT SKU, products.Name, Price, types.Name AS 'Type' 
            FROM products INNER JOIN types ON products.Type = types.ID
            ORDER BY types.Name";
        $conn = $this->connect();
        $stmt = $conn->prepare($sql);
        $stmt->bind_result($sku, $name, $price, $type);
        $stmt->execute();

        // Creating an array of products
        $products = Array();
        while($stmt->fetch()) {
            $className = "\\Product\\".$type;
            $products[$sku] = new $className($sku, $name, $price);
        }

        return $products;
    }

    protected function getAttribute($sku, $attribute)
    {
        // Executing statement
        $sql = "SELECT attributes.Value
            FROM attributes
            INNER JOIN attribute_names ON attributes.Attribute = attribute_names.ID
            WHERE attribute_names.Name = ?
            AND attributes.SKU = ?";
        $conn = $this->connect();
        $stmt = $conn->prepare($sql);
        $stmt->bind_param('ss', $attribute, $sku);
        $stmt->bind_result($value);
        $stmt->execute();
        $stmt->fetch();

        return $value;
    }

    protected function setAttribute($sku, $attribute, $value)
    {
        // Executing statement
        $sql = "INSERT INTO attributes VALUES(?, GET_attribute_id(?), ?);";
        $conn = $this->connect();
        $stmt = $conn->prepare($sql);
        $stmt->bind_param('sss', $sku, $attribute, $value);
        $stmt->execute();
    }
}