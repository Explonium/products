<?php


namespace Mysqli;


class TypeSwitcher extends Connection
{
    protected function getTypes()
    {
        // Executing statement
        $sql = "SELECT types.Name FROM types ORDER BY types.Name";
        $conn = $this->connect();
        $stmt = $conn->prepare($sql);
        $stmt->bind_result($name);
        $stmt->execute();

        // Creating an array of types
        $types = Array();
        while($stmt->fetch()) {
            array_push($types, $name);
        }

        return $types;
    }
}