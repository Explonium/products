<?php


namespace Product;


class Book extends Product
{
    protected static $type = "Book";

    protected function uploadAttributes($data)
    {
        $this->setAttribute($this->sku, "weight", $data['weight']);
    }

    protected function print_attributes()
    {
        echo "Weight: ".$this->getAttribute($this->sku, "Weight (Kg)");
    }
}