<?php


namespace Product;


class Disc extends Product
{
    protected static $type = "Disc";

    protected function uploadAttributes($data)
    {
        $this->setAttribute($this->sku, "size", $data['size']);
    }

    protected function print_attributes()
    {
        echo "Size: ".$this->getAttribute($this->sku, "Size (MB)");
    }
}