<?php


namespace Product;


class Furniture extends Product
{
    protected static $type = "Furniture";

    protected function uploadAttributes($data)
    {
        $this->setAttribute($this->sku, "height", $data['height']);
        $this->setAttribute($this->sku, "width", $data['width']);
        $this->setAttribute($this->sku, "length", $data['length']);
    }

    protected function print_attributes()
    {
        echo "Dimensions: ".
            $this->getAttribute($this->sku, "Height (cm)")." x ".
            $this->getAttribute($this->sku, "Width (cm)")." x ".
            $this->getAttribute($this->sku, "Length (cm)");
    }
}