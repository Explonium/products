<?php


namespace Product;


abstract class Product extends \Mysqli\Product
{
    // MEMBERS ==========================================
    protected $sku;
    protected $name;
    protected $price;
    protected static $type = "Undefined";

    // METHODS ==========================================
    // CONSTRUCTOR
    function __construct($sku, $name, $price)
    {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
    }

    // SETTERS
    public function uploadProduct($data)
    {
        $this->setProduct($this->sku, $this->name, $this->price, static::$type);
        $this->uploadAttributes($data);
    }

    abstract protected function uploadAttributes($data);

    // PRINT
    public function show()
    {
        echo "<div class='col-3 p-1'>
            <div class='p-2 w-100 h-100 text-dark border border-dark align-content-stretch rounded' style='box-sizing:border-box'>
            <input type='checkbox' name='".$this->sku."' value='check'>
            <h5 class='text-center'>".static::$type."</h5>
            <p>SKU: ".$this->sku."<br>
            Name: ".$this->name."<br>
            Price: ".$this->price."</p>";

        $this->print_attributes();
        echo "</div></div>";
    }

    abstract protected function print_attributes();
}