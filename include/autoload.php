<?php

// This function automatically loads classes
function autoload($className)
{
    // Creating path to the class and requiring it
    require '.\\class\\' . str_replace('\\', DIRECTORY_SEPARATOR, $className) . '.php';
}

spl_autoload_register('autoload');