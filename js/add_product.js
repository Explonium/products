var prev_form = 0;

function update_form(value){
    // Set previous form invisible and current form visible (these are divs, not forms)
    set_visible(prev_form, 'none');
    set_visible(value, 'block');
    prev_form = value;
}

function set_visible(form_id, display){
    // Find form with specified id, if there is no such form, then return
    let form = document.getElementById(form_id.toString());
    if (form === null)
        return;

    // Set form display property and set require attributes for all fields in the form
    form.style.display = display;
    let fields = form.querySelectorAll(".field");
    for (let i = 0; i < fields.length; i++){
        if (display === 'none')
            fields[i].removeAttribute("required");
        else
            fields[i].setAttribute("required", "required");
    }
}

function validate(form_name) {
    // Writing each field name to the array
    let arr = ["price", "size", "weight", "height", "width", "length"];

    // And using it in the loop to check every field
    for (let i = 0; i < arr.length; i++){
        if (!chk_positive(document.forms[form_name][arr[i]]))
            return false;
    }
    return true;
}

function chk_positive(field){
    let value = field.value;

    // Ignore empty fields
    if (value === "")
        return true;
    else
        if (value < 0) {
            alert("This field must be a positive value!");
            field.focus();
            return false;
        }
    return true;
}