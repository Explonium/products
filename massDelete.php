<?php
require_once "include/autoload.php";

// Getting array of products
$products = new Control\Product();
$products->deleteProducts($_POST);

?>
<!-- Redirecting to the main page -->
<script>window.location.replace("index.php")</script>