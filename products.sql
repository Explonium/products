-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Июн 08 2020 г., 21:06
-- Версия сервера: 10.4.10-MariaDB
-- Версия PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `products`
--

DELIMITER $$
--
-- Процедуры
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `ADD_attribute` (IN `SKU` CHAR(9), IN `Attribute` VARCHAR(40), IN `Value` INT(10))  NO SQL
BEGIN
SET @p = (SELECT attribute_names.ID
	FROM attribute_names
    WHERE attribute_names.Field_name = Attribute);

INSERT INTO attributes VALUES(SKU, @p, Value);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ADD_product` (IN `SKU` CHAR(9), IN `Name` VARCHAR(40), IN `Price` FLOAT, IN `Type` VARCHAR(40))  NO SQL
INSERT INTO products VALUES(SKU, Name, Price, GET_type_id(Type))$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `DELETE_product` (IN `SKU` CHAR(9))  NO SQL
BEGIN

DELETE
FROM attributes
WHERE attributes.SKU = SKU;

DELETE
FROM products
WHERE products.SKU = SKU;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_attributes` ()  NO SQL
SELECT attributes.SKU,
	attribute_names.Name,
    attributes.Value
FROM attributes
INNER JOIN products ON attributes.SKU = products.SKU
INNER JOIN types ON products.Type = types.ID
INNER JOIN attribute_names ON attributes.Attribute = attribute_names.ID
AND attribute_names.Type = types.ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_attribute_names` ()  NO SQL
SELECT 
	attribute_names.ID,
	types.Name AS Type,
	attribute_names.Name,
    attribute_names.Field_name
FROM attribute_names
INNER JOIN types ON attribute_names.Type = types.ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_attribute_names_for_type` (IN `Type_ID` INT(3))  NO SQL
SELECT *
FROM attribute_names
WHERE attribute_names.Type = Type_ID
ORDER BY attribute_names.ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_products` ()  NO SQL
SELECT
	products.SKU,
    types.Name AS "Type"
FROM products
INNER JOIN types ON products.Type = types.ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_product_attribute` (IN `SKU` CHAR(9), IN `Att_name` VARCHAR(40))  NO SQL
SELECT attributes.Value
FROM attributes
INNER JOIN attribute_names ON attributes.Attribute = attribute_names.ID
WHERE attribute_names.Name = Att_name
AND attributes.SKU = SKU$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_product_name` (IN `SKU` CHAR(9))  NO SQL
SELECT products.Name
FROM products
WHERE products.SKU = SKU$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_product_price` (IN `SKU` CHAR(9))  NO SQL
SELECT products.Price
FROM products
WHERE products.SKU = SKU$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_product_type` (IN `SKU` CHAR(9))  NO SQL
SELECT types.Name
FROM types
INNER JOIN products ON products.Type = types.ID
WHERE products.SKU = SKU$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_type` (IN `ID` INT(3))  NO SQL
SELECT types.Name
FROM types
WHERE types.ID = ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_types` ()  NO SQL
SELECT *
FROM types
ORDER BY Name$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SEP_price` (IN `Price` FLOAT)  NO SQL
UPDATE products
SET products.Price = Price$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SET_name` (IN `Name` VARCHAR(40))  NO SQL
UPDATE products
SET products.Name = Name$$

--
-- Функции
--
CREATE DEFINER=`root`@`localhost` FUNCTION `GET_type_id` (`Type` VARCHAR(40)) RETURNS INT(3) NO SQL
BEGIN

SELECT types.ID
INTO @output
FROM types
WHERE types.Name = Type;

RETURN @output;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `attributes`
--

CREATE TABLE `attributes` (
  `SKU` char(9) NOT NULL,
  `Attribute` int(3) NOT NULL,
  `Value` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `attributes`
--

INSERT INTO `attributes` (`SKU`, `Attribute`, `Value`) VALUES
('ACD', 3, 80),
('ACD', 4, 100),
('ACD', 5, 200),
('ASD-85673', 3, 99),
('ASD-85673', 4, 99),
('ASD-85673', 5, 99),
('GFU-12324', 1, 4812),
('TRF-21345', 2, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `attribute_names`
--

CREATE TABLE `attribute_names` (
  `ID` int(3) NOT NULL,
  `Type` int(3) NOT NULL,
  `Name` varchar(40) NOT NULL,
  `Field_name` varchar(40) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `attribute_names`
--

INSERT INTO `attribute_names` (`ID`, `Type`, `Name`, `Field_name`) VALUES
(1, 1, 'Size (MB)', 'size'),
(2, 2, 'Weight (Kg)', 'weight'),
(3, 3, 'Height (cm)', 'height'),
(4, 3, 'Width (cm)', 'width'),
(5, 3, 'Length (cm)', 'length');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `SKU` char(9) NOT NULL,
  `Name` varchar(40) NOT NULL,
  `Price` float NOT NULL,
  `Type` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`SKU`, `Name`, `Price`, `Type`) VALUES
('ACD', 'Table', 44.99, 3),
('ASD-85673', 'Sofa', 99.99, 3),
('GFU-12324', 'DVR RW-', 0.35, 1),
('TRF-21345', 'Elven Blood', 12.99, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `types`
--

CREATE TABLE `types` (
  `ID` int(3) NOT NULL,
  `Name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `types`
--

INSERT INTO `types` (`ID`, `Name`) VALUES
(1, 'Disc'),
(2, 'Book'),
(3, 'Furniture');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`SKU`,`Attribute`) USING BTREE,
  ADD KEY `Attribute` (`Attribute`);

--
-- Индексы таблицы `attribute_names`
--
ALTER TABLE `attribute_names`
  ADD PRIMARY KEY (`ID`) USING BTREE,
  ADD KEY `Type` (`Type`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`SKU`),
  ADD KEY `Type` (`Type`);

--
-- Индексы таблицы `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `types`
--
ALTER TABLE `types`
  MODIFY `ID` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `attributes`
--
ALTER TABLE `attributes`
  ADD CONSTRAINT `attributes_ibfk_1` FOREIGN KEY (`SKU`) REFERENCES `products` (`SKU`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `attributes_ibfk_2` FOREIGN KEY (`Attribute`) REFERENCES `attribute_names` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `attribute_names`
--
ALTER TABLE `attribute_names`
  ADD CONSTRAINT `attribute_names_ibfk_1` FOREIGN KEY (`Type`) REFERENCES `types` (`ID`);

--
-- Ограничения внешнего ключа таблицы `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`Type`) REFERENCES `types` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
